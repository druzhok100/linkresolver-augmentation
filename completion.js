/* jshint esversion: 6 */
/*************************************************/
const https = require('https'),
      xml2js = require('xml2js'),
      concat = require('concat-stream'),
      parser = new xml2js.Parser(),
      querystring = require('querystring');

const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-completion',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
/*************************************************/
module.exports = function(req, res, context) {
    var module = {};

    //---- Config
    global.context = context;
    
    log.debug('we are in '+context+' context');

    var confFileName = './config/'+context+'.js';
    
    log.debug('reading config from '+confFileName);

    /* As the default config is used in different contexts the cache
       has to be cleared */
    if ( "default" === context ) {
        delete require.cache[require.resolve(confFileName)];
    }
    var conf = require(confFileName).completion;
    var uresolver = require('./helpers/uresolver.js'),
        crossref = require('./crossref.js'),
        oadoi = require('./oadoi.js');
    //----

    module.resolve = function(openurl, self = false) {
        if ( openurl ) {
            // save the origin openurl for later use
            this._OPEN_URL_REQUEST = openurl;
            log.debug('called with this openurl:');
            log.debug(this._OPEN_URL_REQUEST);
            
            // clean the openurl from some Alma/Primo settings or bad metadata
            // and get the doi
            openurl = _clean_openurl(openurl);
            var doi = _get_doi(openurl);

            var options = uresolver.get_options(openurl);
            log.debug('options calling uresolver');
            log.debug(options);

            var req = https.request(options, (uresolver_res) => {
                log.debug('uresolver statusCode:', uresolver_res.statusCode);

                uresolver_res.pipe(concat(function(buffer) {
                    var strXml = buffer.toString();

                    parser.parseString(strXml, function(err, result) {

                        try {
                            log.debug("try parse");
                            log.debug(typeof result.uresolver_content.context_services[0].context_service);
                            
                            var cto = result.uresolver_content.context_object,
                                parsed_cto = JSON.parse(uresolver.CTO_to_JSON(cto));

                            var today = Date.now();

                            if ( uresolver.service_exist(result.uresolver_content) ) {
                                
                                var services = uresolver.services_to_JSON(result.uresolver_content.context_services);
                                log.debug('services to check: '+services.length);

                                var free_service = services.find(uresolver.service.is_free),
                                    our_service = services.find(uresolver.service.is_our),
                                    gin_service = services.find(uresolver.service.is_gin),
                                    other_service = services.find(uresolver.service.is_other);

                                if ( free_service ) {
                                    if ( our_service ) {
                                        if ( free_service === our_service ) {
                                            log.debug('Our service is free');

                                            if ( free_service.key.Filtered && gin_service && !other_service ) {
                                                log.debug('but filtered so we try Get It Now');
                                                _handle_service(gin_service,openurl,cto,self);
                                            }
                                            else {
                                                _handle_service(our_service,openurl,cto,self);
                                            }
                                        }
                                        else {
                                            var aveUntil = Date.parse( our_service.key['Availability'].replace(/.*(until|till) (\d{4}-\d{2}-\d{2}).*/g,"$2") ),
                                                pubTime;

                                            if ( parsed_cto['rft.year'] && parsed_cto['rft.month'] ) {
                                                pubTime = Date.parse(parsed_cto['rft.year']+'-'+parsed_cto['rft.month']);
                                            }
                                            else if ( "Elsevier" === parsed_cto['rft.publisher'] ) {
                                                log.debug('Elsevier service, but we don\'t know the month');
                                            }

                                            if ( our_service.key['Filter reason'] === "Date Filter" || ( pubTime && ( pubTime > aveUntil ) ) ) {
                                                log.debug('We have a service but it has emabargo or only backfiles. Try to find OA');
                                                _find_oa(openurl, parsed_cto['rft.doi'])
                                                    .then(function(url) {
                                                        log.debug('redirect to oa url');
                                                        _redirect(url, cto);
                                                    }, function(reason) {
                                                        log.debug(reason);

                                                        if ( gin_service && _service_from_proxy(gin_service) ) {
                                                            log.debug('Using Get It Now instead of our service that has embargo or only backfiles' + our_service.key['Filter reason']);
                                                            _redirect(gin_service.resolution_url);
                                                        }
                                                        else {
                                                            log.debug(our_service.key['Filter reason'] + ' redirecting to ill form');
                                                            _redirect(conf.url.illForm+'?'+openurl);
                                                        }
                                                    });
                                            }
                                            else {
                                                if ( our_service ) {
                                                    log.debug("Try our service before any free one");
                                                    log.debug(our_service);
                                                    _handle_service(our_service,openurl,cto,self);
                                                }
                                                else {
                                                    log.debug("There's another service (not ours) that is free. Try that one");
                                                    _handle_service(free_service,openurl,cto,self);
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        log.debug("We don't have a service. Check if we can find OA");
                                        
                                        let doi = uresolver.get_identifier_from_CTO(cto, 'doi');
                                        let pmid = uresolver.get_identifier_from_CTO(cto, 'pmid');

                                        _find_oa(openurl, doi, pmid)
                                            .then(function(url) {
                                                log.debug('redirect to oa url');
                                                _redirect(url, cto);
                                            }, function(reason) {
                                                log.debug(reason);

                                                log.debug('redirect to the ill form');
                                                _redirect(conf.url.illForm+'?'+openurl);
                                            });
                                    }
                                }
                                else if ( our_service ) { 
                                    log.debug("We have a service that's not free");
                                    log.debug(our_service);

                                    if ( our_service.key['Filter reason'] === "Date Filter" ) {
                                        log.debug('We have a non-free service but it has emabargo. Try to find OA');

                                        
                                        _find_oa(openurl, parsed_cto['rft.doi'])
                                            .then(function(url) {
                                                log.debug('redirect to oa url');
                                                _redirect(url, cto);
                                            }, function(reason) {
                                                log.debug(reason);

                                                if ( gin_service && _service_from_proxy(gin_service) ) {
                                                    log.debug('Using Get It Now instead of "our service", ' + our_service.key['Filter reason']);
                                                    _redirect(gin_service.resolution_url);
                                                }
                                                else {
                                                    log.debug(our_service.key['Filter reason'] + ' redirecting to ill form');
                                                    _redirect(conf.url.illForm+'?'+openurl);
                                                }

                                            });
                                    }
                                    else {
                                        log.debug('Handling our service');
                                        _handle_service(our_service,openurl,cto,self);
                                    }
                                }
                                else {
                                    log.debug("We don't have a service, and Alma doesn't know any free. Check if we can find OA anyway");
                                    _find_oa(openurl, parsed_cto['rft.doi'])
                                        .then(function(url) {
                                            log.debug('redirect to oa url');
                                            _redirect(url, cto);
                                        }, function(reason) {
                                            log.debug(reason);

                                            var service_from_proxy = services.find(_service_from_proxy);
                                            
                                            if ( service_from_proxy ) {
                                                log.debug('Selected '+service_from_proxy.key['parser_program']+' from proxy');
                                                
                                                var ava = service_from_proxy.key['Availability'];

                                                // Don't allow Get It Now if there's other services
                                                // Get It Now requires volume to handle a request
                                                if ( "CCC::GIN" === service_from_proxy.key['parser_program'] && services.length > 1 || "undefined" === typeof parsed_cto['rft.volume'] ) {
                                                    var cto_helper = require('./helpers/cto.js');
                                            
                                                    log.debug('redirect to the ill form');
                                                    _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(parsed_cto));
                                                }
                                                else if ( ava ) {
                                                    // we need to check the availability key since the date filter
                                                    // doesn't work outside the ip range of the campus
                                                    log.debug('checking date for proxy service');

                                                    var avaMatch = ava.match(/Available from (\d{4})( volume: (\d*))?( issue: (\d*))?( until (\d{4})( volume: (\d*))?( issue: (\d*))?)?/i);

                                                    if (!avaMatch) {
                                                        avaMatch = ava.match(/Tillgänglig från (\d{4})( volym: (\d*))?( nummer: (\d*))?/i);
                                                    }
                                                    
                                                    var availability = {
                                                        from: {},
                                                        to: {}
                                                    };

                                                    if (avaMatch) {
                                                        if ( avaMatch[1] ) { availability.from.year = avaMatch[1]; }
                                                        if ( avaMatch[3] ) { availability.from.volume = avaMatch[3]; }
                                                        if ( avaMatch[5] ) { availability.from.issue = avaMatch[5]; }
                                                        if ( avaMatch[7] ) { availability.to.year = avaMatch[7]; }
                                                        if ( avaMatch[9] ) { availability.to.volume = avaMatch[9]; }
                                                        if ( avaMatch[11] ) { availability.to.volume = avaMatch[11]; }
                                                        
                                                        if ( availability.from.year && ( parsed_cto['rft.year'] >= availability.from.year ) ) {
                                                            if ( availability.to.year && ( parsed_cto['rft.year'] > availability.to.year ) ) {
                                                                _redirect(conf.url.illForm+'?'+openurl);
                                                            }
                                                            else {
                                                                if ( "CCC::GIN" === service_from_proxy.key['parser_program'] ) {
                                                                    log.debug('Using Get It Now for this request');
                                                                    log.debug(module._OPEN_URL_REQUEST);
                                                                }
                                                                _redirect(service_from_proxy.resolution_url);
                                                            }
                                                        }
                                                        else {
                                                            log.debug('redirecting to ill form');
                                                            _redirect(conf.url.illForm+'?'+openurl);
                                                        }
                                                    }
                                                    else {
                                                        _redirect(service_from_proxy.resolution_url);
                                                    }
                                                }
                                                else {
                                                    log.debug('redirecting to ill form');
                                                    _redirect(conf.url.illForm+'?'+openurl);
                                                }
                                            }
                                            else {
                                                log.debug('redirecting to ill form');
                                                _redirect(conf.url.illForm+'?'+openurl);
                                            }
                                        });
                                }
                            }
                            else {
                                log.debug("didn't find any service");

                                _find_oa(openurl, parsed_cto['rft.doi'], uresolver.get_identifier_from_CTO(cto, 'pmid'))
                                    .then(function(url) {
                                        log.debug('redirect to oa url');
                                        _redirect(url, cto);
                                    }, function(reason) {
                                        log.debug(reason);
                                        log.debug('redirecting to ill form');
                                        _redirect(conf.url.illForm+'?'+openurl);
                                    });
                            }
                        }
                        catch (err) {
                            log.error(err);
                        }

                    });
                    
                }));
            });

            req.on('error', (e) => {
                log.error(e);
            });
            req.end();

        }
        else {
            _redirect(conf.url.servicePage);
        }

        parser.on('error', function(err) { log.error('Parser error' - err); });
    };

    function _clean_openurl(openurl) {
        // <date>: [YYYY-MM-DD|YYYY-MM|YYYY]
        var numerals = require('./helpers/numerals.js');

        var obj = querystring.parse(openurl);

        delete obj.svc_dat;

        if ( obj.date && isNaN(Date.parse(obj.date)) && false === isNaN(obj.date.replace(/.*\s(\d{4})$/,"$1")) ) {
            obj.date = obj.date.replace(/.*\s(\d{4})$/,"$1");
        }

        if ( obj['rft.spage'] && isNaN(obj['rft.spage']) & 0 === numerals.from_roman(obj['rft.spage']) ) {
            delete obj['rft.spage'];
            delete obj['rft.pages'];
        }

        return querystring.stringify(obj);
    }

    function _get_context_key(keys, id) {
        var i = 0;
        while ( i < keys.length ) {
            if ( id === keys[i].$.id ) {
                return keys[i]._;
            }
            i++;
        }
        return;
    }

    function _get_doi(openurl, context_object) {
        var q = querystring.parse( openurl ),
            doi;

        if ( q.id && true === /^doi:/gi.test(q.id) ) {
            doi = q.id.substr(4);
        }
        else if ( q.rft_id ) {
            if ( typeof q.rft_id === "object" ) {
                q.rft_id.forEach(function(rft_id) {
                    if ( true === /^info:doi\//gi.test(rft_id) ) {
                        doi = rft_id.substr(9);
                    }
                });
            }
            else if ( typeof q.rft_id === "string" && true === /^info:doi\//gi.test(q.rft_id) ) {
                doi = q.rft_id.substr(9);
            }
        }

        if ( !doi && context_object ) {
            doi = _get_context_key(context_object[0].keys[0].key,"rft.doi");
        }

        return doi;
    }

    function _handle_service(service,openurl,cto,selfResolved) {
        var url = require('url');

        /* The link with the highest priority has a target url,
           if the link doesn't have a target url try to find an oa location
           before redirecting to resolution url 
        */
        var oCTO = JSON.parse(uresolver.CTO_to_JSON(cto));

        if ( service.target_url ) {
            // check for the link in pubmed if the article is free an we have a pmid
            var pmid = '';
            if ( Array.isArray( oCTO.rft_id ) ) {
                pmid = oCTO.rft_id.find(function(element) {
                    return element.toLowerCase().startsWith("pmid:");
                });
                pmid = pmid ? pmid.match(/^pmid:(\d*)/)[1] : null;
            }
            else if ( "string" === typeof oCTO.rft_id && oCTO.rft_id.toLowerCase().startsWith("pmid:") ) {
                pmid = oCTO.rft_id.toLowerCase().match(/^pmid:(\d*)$/)[1];
            }

            if ( pmid ) {
                var Elink = require('./pubmed/elink.js');
                var elink = new Elink(pmid);
                
                // Is_free needs to be a number
                if ( Number(service.key.Is_free) ) {
                    elink.freeUrl()
                        .then(function(link) {
                            log.debug('using url from pubmed for free fulltext source');
                            _redirect(link, cto);
                        }, function(reason) {
                            log.debug(reason);
                            _find_oa(openurl, oCTO['rft.doi'])
                                .then(function(link) {
                                    log.debug('redirect to oa url');
                                    _redirect(link, cto);
                                }, function(reason) {
                                    log.debug(reason);

                                    
                                    // don't do the HEAD check again 
                                    if ( selfResolved ) {
                                        _handle_target_service(service,openurl,cto,false);
                                    }
                                    else {
                                        _handle_target_service(service,openurl,cto);
                                    }
                                });
                        });
                }
                else {
                    elink.prUrl()
                        .then(function(link) {
                            var serviceTarget = url.parse(service.target_url),
                                target_url = serviceTarget.hostname.endsWith(conf.url.proxyHost) ? url.parse(serviceTarget.search.substring(serviceTarget.search.indexOf('http'))) : serviceTarget;
                                
                            if ( url.parse(link).hostname === url.parse(target_url).hostname ) {
                                _redirect(conf.url.proxyPrefix+link, cto);
                            }
                            else {
                                log.debug('pubmed and alma does not have the same interface this service');
                                _handle_target_service(service,openurl,cto);
                            }
                        }, function(reason) {
                            log.debug(reason);
                            _handle_target_service(service,openurl,cto);
                        });
                }
            }
            else {
                _handle_target_service(service,openurl,cto);
            }

        }
        else if ( 'journal' === oCTO['rft.genre'] ) {
            log.debug('using resolution url for journal');
            _redirect(service.resolution_url);
        }
        else {
            log.debug('the service does note have a target url');
        
            var cto_helper = require('./helpers/cto.js'),
                my_cto = cto_helper.my_cto(cto);

            _find_oa(openurl, oCTO['rft.doi'])
                .then(function(url) {
                    log.debug('redirect to oa url');
                    _redirect(url, cto);
                }, function(reason) {
                    log.debug(reason);
                    if ( _service_from_proxy(service) ) {
                        log.debug('using resolution url from service selected by proxy');
                        _redirect(service.resolution_url);
                    }
                    else {
                        _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(my_cto));
                    }
                });
        }
    }

    function _handle_target_service(service,openurl,cto,head) {
        var oCTO = JSON.parse(uresolver.CTO_to_JSON(cto));

        if ( /[a-z]/i.test(oCTO['rft.issue']) && service.target_url.indexOf(oCTO['rft.issue']) > -1 && "undefined" === typeof oCTO['rft.doi'] ) {
            log.debug('issue is used in linking and contains letters');
            log.debug('there is no doi, so try crossref');
                            
            crossref.get_resource_url(openurl)
                .then(function(url) {
                    _redirect(conf.url.proxyPrefix+url, cto);
                }, function(reason) {
                    log.debug(reason);
                    _resolve_service(service,openurl,cto,head);
                });
        }
        else {
            _resolve_service(service,openurl,cto,head);
        }
    }

    function _service_from_proxy(service) {
        return service.key.proxy_selected === conf.proxyName;
    }


    /**
     * Resolver functions
     */
    function _find_oa(openurl, doi, pmid) {
        return new Promise((resolve, reject) => {
            if ( doi ) {
                oadoi.get_oa_location( doi ).then(function(url) {
                    _check_URL(url)
                        .then(function(response) {
                            resolve(url);
                        }, function(req) {
                            if (pmid) {
                                log.debug('unpaywall didn\'t pass URL check. Fetching link from Pubmed insteda');
                                var Elink = require('./pubmed/elink.js');
                                var elink = new Elink(pmid);

                                elink.freeUrl()
                                    .then(function(link) {
                                        log.debug('tried unpaywall, but got link from pubmed');
                                        resolve(link);
                                    }, function(reason) {
                                        log.debug('neither unpaywall or pubmed has a working link');
                                        resolve(url);
                                    });
                                
                            }
                            else {
                                log.debug('unpaywall\'s link does not seem to work');
                                resolve(url);
                            }
                        });
                }, function(reason) {
                    // Does Crossref have any license information? 
                    crossref.filter_license(doi)
                        .then(function (obj) {
                            try {
                                let license_CC = obj.message.items[0].license.find(function (license) {
                                    return 'vor' === license['content-version'] && license.URL.includes('creativecommons.org')
                                });

                                if (license_CC) {
                                    log.debug('Crossref has a CC license');
                                    resolve(conf.url.proxyPrefix+obj.message.items[0].URL);
                                }
                                else {
                                    log.debug('Crossref doesn\t contain any free license');
                                    reject();
                                }
                            }
                            catch (err) {
                                reject(err);
                            }
                        }, function(err) {
                            log.debug('couldn\'t find free resource from doi');

                            reject(err);
                        });
                });
            }
            else {
                oadoi.get_oa_location_by_openurl( openurl )
                    .then(function(url) {
                        log.debug('got OA location from openurl');
                        resolve(url);
                    }, function(reason) {
                        log.debug('couldn\'t find free resource from openurl');
                        reject(reason);
                    });
            }
        });
    }
    
    function _redirect(pURL, cto, head = true) {
        var url = require('url');

        if ( false === /^http(s)?\:\/\//g.test(pURL) ) {
            pURL = conf.url.servicePage+'?'+pURL;
        }
        var myURL = url.parse(pURL),
            myProxy;

        if ( myURL.hostname.endsWith(conf.url.proxyHost) ) {
            myProxy = myURL.protocol+'//'+myURL.hostname+myURL.pathname;
        }

        var myURLwithoutProxy = myURL.hostname.endsWith(conf.url.proxyHost) ? url.parse(myURL.search.substring(myURL.search.indexOf('http'))) : myURL;

        // Check the status code before redirect
        if ( false === conf.redirectSafe.some(function (e) {
            return myURL.hostname.endsWith(e);
        }) ) {
            var errorpage = require('./error_page.js'),
                cto_helper = require('./helpers/cto.js'),
                my_cto = cto_helper.my_cto(cto);

            if (head) {
                log.debug('let\'s do a HEAD request to check the URL');
                _check_URL(myURLwithoutProxy)
                    .then(function(response) {
                        // Everything is allright and we redirect the user
                        log.debug('status of URL check:', response.statusCode);

                        res.writeHead(302, {
                            'Location': myURL.href
                        });
                        res.end();
                    }, function(reqHead) {
                        log.debug('url check was not successful');

                        if (reqHead.aborted) {
                            log.debug('head request was aborted because of error, take a chance');
                            res.writeHead(302, {
                                'Location': myURL.href
                            });
                            res.end();
                        }
                        else {
                            // check crossref before showing the error page
                            if ( Object.keys(my_cto).length > 0 && my_cto.constructor === Object ) {
                                var my_openurl = cto_helper.cto_to_openurl(my_cto);


                                if ( my_cto.doi && false === reqHead._headers.host.endsWith('doi.org') ) {
                                    log.debug("we have a DOI that haven't been used in the linking, check oadoi");
                                    _find_oa(null, my_cto.doi)
                                        .then(function(url) {
                                            log.debug('got an oa url');
                                            _check_URL(url)
                                                .then(function(res) {
                                                    log.debug('redirect to oa url after check');
                                                    _redirect(url, cto, false); // No need to check the URL again. That would cause an endless loop
                                                }, function(req) {
                                                    log.debug('OA did not pass the URL check. Building error page');
                                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                });
                                        }, function(reqHead) {
                                            //log.debug(reqHead);

                                            // check if the doi is correct
                                            crossref.doi_status(my_cto.doi)
                                                .then(function(statusCode) {
                                                    if ( 404 === statusCode ) {
                                                        log.debug(my_cto.doi +" is not in crossref");

                                                        crossref.get_doi( cto_helper.to_openurl(my_cto) )
                                                            .then(function(doi) {

                                                                if (my_cto.doi != doi) {
                                                                    log.debug('correct doi is ',doi);
                                                                    _find_oa(null, doi)
                                                                        .then(function(url) {
                                                                            log.debug('got oa location from correct doi, redirecting');
                                                                            _redirect(url, cto);
                                                                        }, function(reason) {
                                                                            log.debug(reason);
                                                                            module.resolve('id=doi:'+doi,true);
                                                                        });
                                                                }
                                                                else {
                                                                    log.debug('doi is correct');

                                                                    if (myURL.href.toLowerCase().indexOf(my_cto.doi.toLowerCase()) > -1) {
                                                                        _redirect(myURL.href, cto, false);

                                                                    }
                                                                    else {
                                                                        log.debug('building error page');
                                                                        errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                                    }
                                                                }
                                                            }, function(reason) {
                                                                log.debug(reason);
                                                                errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                            });
                                                    }
                                                    else {
                                                        errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                    }

                                                }, function(reason) {
                                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                });
                                        });
                                }
                                else {
                                    log.debug('checking crossref for '+my_openurl);

                                    crossref.get_resource_url( cto_helper.to_openurl(my_cto) )
                                        .then(function(resource_url) {
                                            /* Redirect if the first DOI was wrong
                                               or if the new DOI goes to the same host */
                                            if ( reqHead._headers.host.endsWith('doi.org') ||
                                                 ( reqHead._headers.host === url.parse(resource_url).host ) ||
                                                 ( /^.*\..*\.\w*$/.test(url.parse(resource_url).host) && url.parse(resource_url).host.endsWith(reqHead._headers.host.substr(reqHead._headers.host.indexOf("."))) ) ) {
                                                var location = myProxy ? myProxy + '?url=' + resource_url : resource_url;
                                                _redirect(location);
                                            }
                                            else {
                                                log.debug('resource url and host not the same');

                                                // Try to correct the start page
                                                if ( parseInt(my_cto.spage) === 2 &&
                                                     my_cto.spage === my_cto.epage ) {
                                        
                                                    log.debug('spage and epage both 2');
                                                    log.debug('trying to move spage to 1');
                                        
                                                    my_cto.spage = parseInt(my_cto.spage)-1;
                                                    my_cto.pages = my_cto.spage+'-'+my_cto.epage;
                                        
                                                    module.resolve(cto_helper.cto_to_openurl(my_cto),true);
                                                }
                                                else {
                                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                }
                                            }
                                        }, function(reason) {
                                            log.debug(reason);

                                            // check for pubmed id and then try to resolve it that way
                                            log.debug('got no url from crossref, checking for pubmed id');

                                            var Ecitmatch = require('./pubmed/ecitmatch.js'),
                                                ecitmatch = new Ecitmatch();

                                            ecitmatch.pmidForCitation(my_cto.jtitle,my_cto.year,my_cto.volume,my_cto.spage,my_cto.aulast)
                                                .then(function(pmid) {
                                                    log.debug('got '+pmid+' from ecitmatch');
                                                    module.resolve('id=pmid:'+pmid,true);
                                                }, function(reason) {
                                                    log.debug(reason);
                                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                });                                   
                                        });
                                }
                            }
                            else {
                                log.debug('empty cto, redirecting to ' + myURL.href);
                                log.debug('request was ' + module._OPEN_URL_REQUEST);
                                res.writeHead(302, {
                                    'Location': myURL.href
                                });
                                res.end();
                            }
                        }
		    });
            }
            else {
                log.debug('Redirect without HEAD check. We asume it is already checked');
                res.writeHead(302, {
                    'Location': myURL.href
                });
                res.end();
            }
	}	
	else {
	    log.debug('ExLibris\' domain, Redirect without any check');
            res.writeHead(302, {
                'Location': myURL.href
            });
            res.end();
        }
    }

    function _resolve_unstable_service(service, openurl_old, cto) {
        return new Promise((resolve, reject) => {
            /* TODO:
               - Get the DOI from the CTO: JSON.parse(uresolver.CTO_to_JSON(cto)) */
            
            var doi = _get_doi(openurl, cto),
                parsed_cto = JSON.parse(uresolver.CTO_to_JSON(cto)),
                openurl = uresolver.CTO_to_OpenURL(cto);

            switch ( service.key.parser_program ) {

            case 'ELSEVIER::SCIENCE_DIRECT':
                if (conf.boycott.length > 0) {
                    let boycott = conf.boycott.find(function(b) {
                        return 'ELSEVIER::SCIENCE_DIRECT' === b.parser_program;
                    });

                    if (boycott && parsed_cto['rft.month']) {
                        if (new Date(parsed_cto['rft.year']+'-'+parsed_cto['rft.month']) >= boycott.date) {
                            log.debug('we don\'t have access to this due to boycott');

                            if (parsed_cto['rft.doi']) {
                                log.debug('trying to find OA');
                                _find_oa(openurl, parsed_cto['rft.doi'])
                                    .then(function(url) {
                                        resolve(url);
                                    }, function(reason) {
                                        log.debug(reason);

                                        resolve(conf.url.illForm+'?'+openurl);
                                    });
                            }
                        }
                        else {
                            resolve(service.target_url);
                        }
                    }
                    else {
                        resolve(service.target_url);
                    }
                }
                
                break;

            case 'OVID::Journals':

                // See for example 10.1097/01.NAJ.0000334962.21371.66 why this is needed
                log.debug('parsing Ovid');
                
                var ovid = require('./parsers/ovid.js')(openurl, doi),
                    ovid_target_url = ovid.get_target_url(service.target_url);

                if ( ovid_target_url ) {
                    resolve(conf.url.proxyPrefix+ovid_target_url);
                }
                else {
                    log.debug('missing ovid_target_url');
                    resolve(conf.url.servicePage+'?'+openurl);
                }
            
                break;

            case 'OUP::OUP':
                if ( doi ) {
                    resolve(conf.url.proxyPrefix+'https://doi.org/'+doi);
                }
                else {
                    crossref.get_resource_url(openurl)
                        .then(function(url) {
                            resolve(conf.url.proxyPrefix+url);
                        }, function(reason) {
                            log.debug(reason);
                            resolve(service.target_url);
                        });
                }
                
                break;
                
            case 'Bulk::BULK':
                if ( doi && true === /\/content\/by\/year$/gi.test(service.target_url)  ) {
                    resolve(conf.url.proxyPrefix+'https://doi.org/'+doi);
                }
                else {
                    resolve(service.target_url);
                }

                break;

            default:
                resolve(service.target_url);
            }
        });
    }

    function _resolve_service(service,openurl,cto,head) {
        var resolution;
        
        /* Some parser programs needs to be adjusted */
        var unstable_parser = ["Bulk::BULK",
                               "CCC::GIN",
                               "OVID::Journals",
                               "OUP::OUP"];

        if (conf.boycott) {
            conf.boycott.forEach(function (element) {
                unstable_parser.push(element.parser_program);
            });
        }

        if ( unstable_parser.includes(service.key.parser_program) ) {
            log.debug('get resolution for '+service.key.parser_program);
            resolution = _resolve_unstable_service(service, openurl, cto, head);
        }
        else {
            log.debug("resolve to uresolver's target url for " + service.key.parser_program);
            resolution = Promise.resolve(service.target_url);
        }

        resolution.then(function(url) {
            log.debug('resolved resolution promise');
            _redirect(url, cto, head);
        }, function(reason) {
            log.debug(reason);
            log.debug('redirecting to service page');
            _redirect(openurl);
        });
    }

    function _check_URL(urlToCheck) {
        return new Promise((resolve, reject) => {
            var url = require('url'),
                http;

            var myURL = url.parse(urlToCheck);
            var options = {method: 'HEAD', host: myURL.hostname, path: myURL.path};

            switch (myURL.protocol) {
            case 'https:':
                http = require('https');
                break;
            case 'http:':
                http = require('http');
                    break;
                default:
                    break;
                }

                log.debug(`Checking ${myURL.href}`);
                log.debug(options);

                var req = http.request(options, function(res) {
                    if ( ( res.statusCode < 200 || res.statusCode > 399 ) && false === [403,405,503].includes(res.statusCode) ) {
                        log.debug('HEAD request gave', res.statusCode);
                        reject(req);
                    }
                    else {
                        resolve(res);
                    }
                });

                req.setTimeout(1500, function() {
                    log.debug('timeout on head request');
                    reject(req);
                    req.abort();
                });

                req.on("error", (err) => {
                    console.log("Error: " + err.message);
                    reject();
                });

                req.end();
            });
        }
        //---

        return module;
    };
