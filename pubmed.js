/* jshint esversion: 6 */
/*************************************************/
const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-pubmed',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });

const https = require('https'),
      xml2js = require('xml2js'),
      concat = require('concat-stream'),
      parser = new xml2js.Parser();

const Eutils = require('./pubmed/eutils.js');

/*************************************************/

module.exports = function(req, res, context) {
    var module = {};
    var completion = require('./completion.js')(req, res, context);

    module.request = function(pmid) {

        var eutils = new Eutils('efetch', {db: 'pubmed', retmode: 'xml', id: pmid});

        var location;

        var efetch_req = https.request(eutils.options, (efetch_res) => {

            try {
                log.debug('fetching data for PMID '+pmid);

                efetch_res.pipe(concat(function(buffer) {
                    var str = buffer.toString(),
                        obj;

                    parser.parseString(str, function(err, result) {
                        var i;


                        // Handle if there's no data in Pubmed
                        if ( !result || !result.PubmedArticleSet || result.PubmedArticleSet === '\n' ) {
                            log.error('no data in pubmed for '+pmid);
                            if (err ) {
                                log.error(err);
                            }
                            else if ( result ) {
                                log.error(result);
                            }
                            completion.resolve();
                            return;
                        }

                        var openurl, genre;

                        // A uniqe ID for article, chapter, book or journal
                        var id = {};

                        if ( result.PubmedArticleSet.PubmedArticle ) {
                            genre = 'article';
                            obj = result.PubmedArticleSet.PubmedArticle[0].PubmedData[0];
                            id.type = ['doi','pmc'];
                        }
                        else if ( result.PubmedArticleSet.PubmedBookArticle ) {
                            genre = 'book';
                            obj = result.PubmedArticleSet.PubmedBookArticle[0].BookDocument[0];
                            id.type = ['bookaccession']; // bookshelf id
                        }

                        log.info('resolving '+genre);

                        id.type.forEach(function (type, e) {
                            i = 0;
                            while ( i < obj.ArticleIdList[0].ArticleId.length && typeof id[type] === 'undefined' ) {
                                if ( obj.ArticleIdList[0].ArticleId[i].$.IdType === type ) {
                                    id[type] = obj.ArticleIdList[0].ArticleId[i]._;
                                }
                                i++;
                            }
                        });

                        // Free PMC articles and books from NCBI Bookshelf can be redirect
                        // on the fly if there's no embargo on them
                        if ( id.pmc ) {
                        
                            var pubmed_pubdate = obj.History[0].PubMedPubDate,
                                pmc_releasedate;

                            i = 0;
                            while ( i < pubmed_pubdate.length && !pmc_releasedate ) {
                            
                                if ( "pmc-release" === pubmed_pubdate[i].$.PubStatus ) {
                                    pmc_releasedate = new Date( pubmed_pubdate[i].Year, pubmed_pubdate[i].Month, pubmed_pubdate[i].Day );
                                }
                                i++;
                            }

                            // Om inget releasedatum för PMC finns är det en fri artikel utan embargo
                            if ( !pmc_releasedate || (  pmc_releasedate.valueOf() <= new Date().valueOf() ) ) {
                                log.info('resolve to pmc');
                                location = 'https://www.ncbi.nlm.nih.gov' + '/pmc/articles/' + id.pmc + '/';
                            }
                        
                        }
                        else if ( id.bookaccession ) {
                            log.info('resolve to bookshelf');
                            location = 'https://www.ncbi.nlm.nih.gov' + '/books/' + id.bookaccession + '/';
                        }

                        if ( location ) {
                            res.writeHead(302, {
                                'Location': location
                            });
                            res.end();
                            return;
                        }

                        if ( genre === 'article' ) {
                            // important parts in an OpenURL request
                            var article, journal, pages, aulast, aufirst, aucorp;
                            
                            var pubmedArticle = result.PubmedArticleSet.PubmedArticle[0],
                                medlineCitation = pubmedArticle.MedlineCitation;
                            medlineCitation = medlineCitation ? medlineCitation[0] : null;

                            if ( medlineCitation ) {
                                var medlineArticleCitation = medlineCitation.Article[0];

                                // Get the DOI from Medline if it's not in the Pubmed data
                                if ( !Boolean(id.doi) && medlineArticleCitation.ELocationID ) {
                                    i = 0;
                                    while ( i < medlineArticleCitation.ELocationID.length && !Boolean(id.doi) ) {
                                        if ( 'doi' === medlineArticleCitation.ELocationID[i].$.EIdType && 'Y' === medlineArticleCitation.ELocationID[i].$.ValidYN ) {
                                            id.doi = medlineArticleCitation.ELocationID[i]._;
                                        }
                                        i++;
                                    }
                                }

                                article = {
                                    title: medlineArticleCitation.ArticleTitle[0]
                                };

                                // Journal
                                var medlineJournalCitation = medlineArticleCitation.Journal[0];

                                journal = {
                                    title: medlineJournalCitation.Title ? medlineJournalCitation.Title[0] : '',
                                    issn: medlineCitation.MedlineJournalInfo[0].ISSNLinking ? medlineCitation.MedlineJournalInfo[0].ISSNLinking[0] : '' // Or medlineJournalCitation.ISSN[0]._,
                                };                        

                                journal.volume = medlineJournalCitation.JournalIssue[0].Volume ? medlineJournalCitation.JournalIssue[0].Volume[0] : '';
                                journal.issue = medlineJournalCitation.JournalIssue[0].Issue ? medlineJournalCitation.JournalIssue[0].Issue[0] : '';

                                // Pagination and author
                                pages = medlineCitation.Article[0].Pagination ? medlineCitation.Article[0].Pagination[0].MedlinePgn[0] : '';

                                // Author
                                if ( medlineCitation.Article[0].AuthorList ) {
                                    aulast = medlineCitation.Article[0].AuthorList[0].Author[0].LastName ? medlineCitation.Article[0].AuthorList[0].Author[0].LastName[0] : '';
                                    aufirst = medlineCitation.Article[0].AuthorList[0].Author[0].ForeName ? medlineCitation.Article[0].AuthorList[0].Author[0].ForeName[0] : '';
                                    aucorp = medlineCitation.Article[0].AuthorList[0].Author[0].CollectiveName ? medlineCitation.Article[0].AuthorList[0].Author[0].CollectiveName[0] : '';
                                }

                                if ( medlineJournalCitation.JournalIssue ) {
                                    var pubDate = medlineJournalCitation.JournalIssue[0].PubDate[0];
                                    if ( pubDate.Year ) {
                                        journal.year = pubDate.Year[0];
                                    }
                                    else if ( pubDate.MedlineDate ) {
                                        journal.year = pubDate.MedlineDate[0].replace(/^.*([0-9]{4}).*$/g,"$1");
                                    }
                                }

                            }

                            // Build an OpenURL
                            openurl = 'id=pmid:'+encodeURIComponent(pmid);
                            if ( id.doi ) {
                                /* We trust the metadata in Pubmed and Medline */
                                openurl += '&id=doi:'+encodeURIComponent(id.doi);
                            }

                            // Metadata we got from Medline
                            if ( medlineCitation ) {
                                openurl += '&jtitle='+encodeURIComponent(journal.title)+'&issn='+encodeURIComponent(journal.issn)+'&year='+encodeURIComponent(journal.year)+'&volume='+encodeURIComponent(journal.volume)+'&issue='+encodeURIComponent(journal.issue)+'&atitle='+encodeURIComponent(article.title)+'&pages='+encodeURIComponent(pages)+'&aulast='+encodeURIComponent(aulast)+'&aufirst='+encodeURIComponent(aufirst)+'&aucorp='+encodeURIComponent(aucorp);
                            }

                        }
                        else if ( genre === 'book' ) {
                            var book = {};
                            return;
                        }
                        else {
                            return;
                        }

                        log.debug('from pubmed complete '+openurl);
                        completion.resolve( openurl );
                    });
                }));
            }
            catch(err) {
                log.error(err);
            }

        });
        
        efetch_req.end(); 

        efetch_req.on('error', (e) => {
            log.error('Pubmed error' + e);
        });

        parser.on('error', function(err) { log.error('Parser error' - err); });

    };

    return module;
};
