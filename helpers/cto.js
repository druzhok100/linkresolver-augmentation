var CTO = function() {};

CTO.prototype.my_cto = function(cto) {
    // Returnerar ett tomt objekt om cto saknas
    return this._parse_cto(cto);
};

CTO.prototype.to_openurl = function(cto, context) {
    return this.cto_to_openurl(cto, context);
};

CTO.prototype.cto_to_openurl = function(cto, context) {
    var openurl = '';
    var cto_keys = Object.keys(cto);

    for (var i=0; i<cto_keys.length; i++) {
        if ("undefined" != typeof cto[cto_keys[i]]) {
            openurl += cto_keys[i]+'='+encodeURIComponent(cto[cto_keys[i]])+'&';
        }
    }

    if ( context ) {
        openurl += 'context='+encodeURIComponent(context);
    }
    
    return openurl;
};

CTO.prototype._parse_cto = function(cto) {
    if ( "undefined" === typeof cto ) {
        return {}; // Lägg till bättre felhantering här
    }
    
    var keys = cto[0].keys[0].key;
    var parsed_cto = {};

    var i = 0;
    while ( i < keys.length ) {
        if ( "undefined" === typeof parsed_cto[keys[i].$.id] ) {
            parsed_cto[keys[i].$.id] = keys[i]._;
        }
        i++;
    }

    var my_cto = {
        // ID
        mms_id: parsed_cto["rft.mms_id"],
        inventory_id: parsed_cto.inventory_id,
        rfr_id: parsed_cto["rfr_id"],
        rft_id: parsed_cto["rft_id"],
        primo_docID: parsed_cto["sfx.primo_docID"],
        // Genre
        genre: parsed_cto["rft.genre"],
        // Titel
        btitle: parsed_cto["rft.btitle"],
        jtitle: parsed_cto["rft.jtitle"],
        stitle: parsed_cto["rft.stitle"],
        title: parsed_cto["rft.title"],
        // Författare

        // Utgivning
        place: parsed_cto["rft.place"],
        pub: parsed_cto["rft.pub"],
        date: parsed_cto["rft.date"],
        year: parsed_cto["rft.year"],
        month: parsed_cto["rft.month"],
        day: parsed_cto["rft.day"],
        volume: parsed_cto["rft.volume"],
        issue: parsed_cto["rft.issue"],
        // ISSN
        issn: parsed_cto["rft.issn"],
        eissn: parsed_cto["rft.eissn"],
        // Sidor

    };

    /* Permalänk-struktur i Primo: 
       TN_ sfx.primo_docTAG + sfx.primo_docID
       https://lnu-se-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=TN_wos000187279800002&context=PC&vid=primo-custom-lnu&search_scope=default_scope&tab=default_tab&lang=sv_SE */

    /*** Putsa på metadatan ***/
    // Författare
    if ( /(\w*)\,\s(\w*)/.test(parsed_cto["rft.aulast"]) ) {
        my_cto.aulast = parsed_cto["rft.aulast"].replace(/(\w*)\,\s(\w*)/i,"$1");
        my_cto.aufirst = parsed_cto["rft.aulast"].replace(/(\w*)\,\s(\w*)/i,"$2");
        my_cto.au = parsed_cto["rft.aulast"];
    }
    else {
        my_cto.aulast = parsed_cto["rft.aulast"];
        if ( parsed_cto["rft.aufirst"] ) {
            my_cto.aufirst = parsed_cto["rft.aufirst"];
        }
        if ( parsed_cto["rft.auinit"] ) {
            my_cto.auinit1 = parsed_cto["rft.auinit"];
        }
        if ( parsed_cto["rft.au"] ) {
            my_cto.au = parsed_cto["rft.au"];
        }
    }

    if ( my_cto.au && my_cto.au.match(/\,\s\w$/i) ) {
        my_cto.au = my_cto.au + '.';
    }

    // Titlar
    if ( parsed_cto["rft.atitle"] ) {
        my_cto.atitle = parsed_cto["rft.atitle"].replace(/\.$/,"");
    }
    if ( parsed_cto["rft.jtitle"] ) {
        my_cto.jtitle = parsed_cto["rft.jtitle"].replace(/\.$/,"").replace(/\s\/$/,"");
    }

    
    // Volym och numrering
    if ( my_cto.volume && isNaN(my_cto.volume) ) {
        var numerals = require('./numerals.js');
        my_cto.volume = numerals.from_roman(my_cto.volume);
    }

    if ( parsed_cto["rft.issue"] ) {
        my_cto.issue = parsed_cto["rft.issue"];
    }

    // Sidangivele
    if ( parsed_cto["rft.spage"] ) {
        my_cto.spage = parsed_cto["rft.spage"].replace(/(\d*)\-(\d*)/i,"$1");
    }

    if ( parsed_cto["rft.epage"] ) {
        my_cto.epage = parsed_cto["rft.epage"].replace(/(\d*)\-(\d*)/i,"$2");
    }

    if ( my_cto.spage && parsed_cto["rft.pages"] && parsed_cto["rft.pages"].indexOf(my_cto.spage) === 0 ||
         "undefined" === typeof my_cto.spage && parsed_cto["rft.pages"] ) {
        my_cto.pages = parsed_cto["rft.pages"];
    }

    // DOI m.m.
    if ( parsed_cto["rft.doi"] ) {
        //my_cto.rft_id.replace(/^doi:(.*)/,"$1")
        my_cto.doi = parsed_cto["rft.doi"];
    }

    return my_cto;
};

module.exports = new CTO();
