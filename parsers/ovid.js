/*************************************************/
const querystring = require('querystring');

const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-ovid',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
/*************************************************/
module.exports = function(openurl, doi) {
    var module = {};

    module.get_target_url = function(target_url) {
        var uresolver_target = {
            proxified_url: "string" === typeof target_url ? target_url : target_url[0]
        };

        console.log(uresolver_target);

        var match = uresolver_target.proxified_url.match(/(https?:\/\/ovid.*\?)(.*$)/i);

        uresolver_target.url = match[0];
        uresolver_target.querystring = match[2];
        
        var ovid_base_url = match[1],
            q = querystring.parse( uresolver_target.querystring );

        // Vi bygger en egen sökning om ExLibris inte har fulltext-länk
        // men även om det finns doi, eftersom uresolver inte använder doi för Ovid
        if ( doi || 'toc' === q.PAGE || 'titles' === q.PAGE ) {
            log.debug('building ovid search');
            var ovid_search = _get_ovid_search(openurl, doi);
            
            if ( ovid_search ) {
                delete q.AN;
                q.CSC = 'Y';
                q.PAGE = 'fulltext';
                q.SEARCH = ovid_search;

                return ovid_base_url + querystring.stringify(q);
            }
            else {
                return uresolver_target.url;
            }
        }
	else if ( 'fulltext' === q.PAGE ) {
            // Exlibris bygger förhoppningsvis fulltextlänkar så här: "0028-3878".is and "87".vo and "3".ip and "299".pg
            log.debug('uresolver has fulltext link');

	    return uresolver_target.url;
	}
	else {
	    return;
	}

    };

    function _get_ovid_search(openurl, doi) {
        if ( doi ) {
            log.debug('ovid search based on doi:'+doi);
            return '"'+doi+'".di';
        }
        else {
            log.debug('ovid search based on openurl:'+openurl);
            
            var openurl_request = querystring.parse( openurl ),
                issn, volume, issue, spage, atitle, aulast,
                illegal_char = /[\#\"\&\%\:\;\.\/\(\)]/ig, 
                stop_words = /(^|\s)([^\s]*[α-ω][^\s]*?)(\s|$)/ig;

            issn = openurl_request['rft.issn'] || openurl_request.issn || '';
            volume = openurl_request['rft.volume'] || openurl_request.volume || '';
            atitle = openurl_request['rft.atitle'] || openurl_request.atitle || '';
            issue = openurl_request['rft.issue'] || openurl_request.issue || '';
            spage = openurl_request['rft.spage'] || openurl_request.spage || '';
            aulast = openurl_request['rft.aulast'] || openurl_request.aulast || '';

            if ( atitle && aulast ) {
                // Ta bort ogiltiga tecken och stoppord från artikeltiteln innan sökningen byggs
                atitle = atitle.replace(illegal_char,'');
                atitle = atitle.replace(stop_words,'');
            
                if ( issn ) {
                    if ( volume && issue && spage ) {
                        return '('+issn+').is and ('+volume+').vo and ( ('+atitle+').ti or ( ('+issue+').ip and ( ('+spage+').pg or ('+aulast+').au ) ) )';
                    }
                    else {
                        return '('+issn+').is and ( ('+atitle+').ti or ('+aulast+').au )';
                    }
                }
            }
            else {
                log.debug("couldn't build ovid search from "+openurl);
                return;
            }
        }

    }

    return module;
};
