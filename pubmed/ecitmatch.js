/* jshint esversion: 6 */
const Eutils = require('./eutils.js');
const https = require('https');

var Ecitmatch = function() {
    this._eutils = new Eutils('ecitmatch', {db: 'pubmed', retmode: 'xml'});
};

Ecitmatch.prototype.pmidForCitation = function(jtitle, year, volume, spage, aulast) {
    this._eutils.options.path += '&bdata='+encodeURIComponent(jtitle)+'|'+year+'|'+volume+'|'+spage+'|'+encodeURIComponent(aulast);

    return new Promise((resolve, reject) => {
        var req = https.request(this._eutils.options, (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });
            
            res.on('end', () => {
                var pmid = data.replace(/.*\|\d*\|.*\|.*\|.*\|.*\|(\.*)/,"$1").trim();

                if ( /^\d*$/.test(pmid) ) {
                    resolve(pmid);
                }
                else {
                    reject(pmid);
                }
            });
        
        }).on('error', (err) => {
            console.log(err);
        });

        req.end();
    });
};

module.exports = Ecitmatch;
