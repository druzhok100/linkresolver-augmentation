/* jshint esversion: 6 */
/*************************************************/
const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-pubmed',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
/*************************************************/
const querystring = require('querystring');

var confFileName = '../config/'+context+'.js';
delete require.cache[require.resolve(confFileName)];
var conf = require(confFileName);
log.debug('eutils reading config from '+confFileName);

var Eutils = function(api, par) {
    var path = {
        base: '/entrez/eutils/',
        suffix: '&tool='+conf.eutils.tool+'&email='+conf.admin.email,
    };
    
    this.options.path =
        path.base +
        api + '?' + querystring.stringify(par) +
        path.suffix
};

Eutils.prototype.options = {
    hostname: 'eutils.ncbi.nlm.nih.gov',
    method: 'GET'
};

module.exports = Eutils;
