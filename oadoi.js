/*************************************************/
const https = require('https'),
      crossref = require('./crossref.js');

const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-oadoi',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
/*************************************************/
var Oadoi = function () {};

Oadoi.prototype.get_oa_location = function(doi) {
    var options = {
        hostname: 'api.oadoi.org',
        path: '/v2/'+doi+'?email=jakob.nylinnilsson@lnu.se'
    };

    return new Promise((resolve,reject) => {
        var req = https.request(options, (res) => {
            log.debug('query to oadoi');

            if (res.statusCode < 200 || res.statusCode > 299) {
                reject(new Error('Failed to get data from oadoi, status code: ' + res.statusCode));
            }

            var body = '';
            
            res.on('data', function(chunk){
                body += chunk;
            });

            res.on('end', function(){
                var response = JSON.parse(body);
                if ( response.error ) {
                    reject(response.message);
                }
                else if ( response.best_oa_location ) {
                    log.debug('got an oa location');

                    if ( response.best_oa_location.url_for_landing_page ) {
                        resolve(response.best_oa_location.url_for_landing_page);
                    }
                    else {
                        resolve(response.best_oa_location.url_for_pdf);
                    }
                }
                else {
                    reject('no oa available');
                }
            });

        });

        req.end();

        req.setTimeout(3000, function() {
            log.debug('timeout when quering oadoi.org');
            req.abort();
        });

        req.on('error', (err) => reject(err));
    });
};

Oadoi.prototype.get_oa_location_by_openurl = function(openurl) {
    var that = this;
    return new Promise((resolve, reject) => {
        crossref.get_doi(openurl)
            .then(function(doi) {
                log.debug('i have a doi :',doi);
                that.get_oa_location(doi)
                    .then(function(url) {
                        resolve(url);
                    }, function(reason) {
                        reject(reason);
                    });
            }, function(reason) {
                reject(reason);
            });
    });
};

module.exports = new Oadoi();
